FROM python:3.6-alpine

COPY ./app /app
WORKDIR /app

CMD python /app/app.py
