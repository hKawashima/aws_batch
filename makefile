ECR_REPOSITORY_NAME=api/test
AWS_PROFILE=default
ACCOUNTID=348718260634
REGION=ap-northeast-1
REPOSITORY_URI=${ACCOUNTID}.dkr.ecr.ap-northeast-1.amazonaws.com/api/test

create-repository:
	aws ecr create-repository --repository-name ${ECR_REPOSITORY_NAME}

# Login to AWS registry (must have docker running)
docker/registry/login:
	$$(aws ecr get-login --no-include-email --region ${REGION} --profile=${AWS_PROFILE})

# Build docker target
docker/build:
	docker build -f Dockerfile --no-cache -t ${ECR_REPOSITORY_NAME} .

# Tag docker image
docker/image/tag:
	docker tag ${ECR_REPOSITORY_NAME}:latest ${REPOSITORY_URI}:latest

# Push to registry
docker/registry/push:
	docker push ${REPOSITORY_URI}:latest

# Build docker image and push to AWS registry
docker/build-and-push: docker/registry/login docker/build docker/image/tag docker/registry/push 
all: docker/build-and-push
